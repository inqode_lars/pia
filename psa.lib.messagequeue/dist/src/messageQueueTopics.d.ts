export declare enum MessageQueueTopic {
    PROBAND_CREATED = "proband.created",
    PROBAND_DELETED = "proband.deleted",
    PROBAND_DEACTIVATED = "proband.deactivated",
    PROBAND_LOGGED_IN = "proband.logged_in",
    PROBAND_REGISTERED = "proband.registered",
    PROBAND_EMAIL_VERIFIED = "proband.email_verified",
    COMPLIANCE_CREATED = "compliance.created",
    QUESTIONNAIRE_INSTANCE_RELEASED = "questionnaire_instance.released"
}
